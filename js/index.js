// a ) On check/uncheck of header checkbox mark all checkbox of table check/uncheck
document.querySelector("#selectAll").addEventListener("click", function () {
  document.querySelectorAll(".user-checkbox").forEach((user) => {
    user.checked = this.checked;
  });
});

// b ) On click of delete button of particular row change the color of that row and mark checkbox of that row as check
const tBody = document.querySelector("tbody");
tBody.addEventListener("click", (e) => {
  if (e.target.parentElement.classList[0] === "delete") {
    e.target.parentElement.parentElement.parentElement.classList.add(
      "bg-deleted"
    );
    e.target.parentElement.parentElement.parentElement.firstElementChild.children[0].checked = true;
  }
});

// c ) On check on header delete button check if any row selected or not, if not then show alert that kindly select at least one row
const headerDelete = document.querySelector("#header-delete");

headerDelete.addEventListener("click", function (e) {
  let totalChecked = 0;
  allCheckBox = document.querySelectorAll(".user-checkbox");
  allCheckBox.forEach((user) => {
    if (user.checked) {
      totalChecked += 1;
    }
  });
  if (totalChecked === 0) {
    alert("select at least one row...");
  }
});

// d ) On click on header edit button check more than one row selected or not, if selected then show alert that you can edit one record at time
const headerEdit = document.querySelector("#header-edit");

headerEdit.addEventListener("click", function (e) {
  let totalChecked = 0;
  allCheckBox = document.querySelectorAll(".user-checkbox");
  allCheckBox.forEach((user) => {
    if (user.checked) {
      totalChecked += 1;
    }
  });
  if (totalChecked > 1) {
    alert("you can edit one record at a time...");
  }
  if (totalChecked === 0) {
    alert("select at least one row...");
  }
});

// e ) Make One button with name even and put it in header, when user click on that change color of even row (make class and add that class to row)
headerEven = document.querySelector("#header-even");
headerEven.addEventListener("click", function (e) {
  const tr = document.querySelector("tbody").children;
  for (i = 1; i <= tr.length; i++) {
    if (i % 2 === 0) {
      tr[i - 1].classList.add("bg-secondary");
    }
  }
  e.preventDefault();
});

// f ) Add one button with name duplicate in action column, on click on that make duplicate row.
tBody.addEventListener("click", function (e) {
  if (e.target.parentElement.classList[0] === "duplicate") {
    const tr = e.target.parentElement.parentElement.parentElement.innerHTML;
    let newTr = document.createElement("tr");
    newTr.innerHTML = tr;
    this.appendChild(newTr);
  }
});

// g ) Add one button with name count and put in header, when user click on that show the total number of row and column
headerCount = document.querySelector("#header-count");
headerCount.addEventListener("click", function (e) {
  const totalRows = document.querySelectorAll("tbody tr");
  alert(`Total ${totalRows.length} records found...`);
  e.preventDefault();
});

// h ) Make first column value bold
document
  .querySelectorAll("tr td:nth-child(2)")
  .forEach((ele) => (ele.style.fontWeight = "bold"));

// i ) On click on edit button display values of that row in one div
tBody.addEventListener("click", function (e) {
  if (e.target.parentElement.classList[0] === "edit") {
    const tr = e.target.parentElement.parentElement.parentElement.children;

    document.querySelector(".user-info").innerHTML = `
        <h2>You Have Selected </h2>
        <p><strong>Name :- </strong>${tr[1].textContent}</p>
        <p><strong>Roll No. :- </strong>${tr[2].textContent}</p>
        <p><strong>Class :- </strong>${tr[3].textContent}</p>
    `;
  }
});

// j ) Delete all table rows except first when user click on header delete button
document
  .querySelector("#header-delete-all")
  .addEventListener("click", function () {
    const tableRows = document.querySelectorAll("tr");
    for (i = 2; i < tableRows.length; i++) {
      tableRows[i].remove();
    }
  });

// K ) Limit character input in the text area including count.
let searchText = "";
document.getElementById("search").addEventListener("keyup", function (e) {
  if (this.value.length <= 15) {
    const remains = 15 - this.value.length;
    document.querySelector(".remains").textContent = remains;
    searchText = this.value;
  }
  this.value = searchText;
});

// a ) Get value of selected option
document.querySelector("#subject").addEventListener("change", function (e) {
  document.querySelector(".selected-subject").textContent = this.value;

  // b ) Make all option disable except selected
  const options = this.options;
  for (i = 0; i < options.length; i++) {
    if (options[i].value !== this.value) {
      options[i].disabled = true;
    }
  }
});
// c ) Make one text box with button add, when user click on that button add new option with text box value
document.querySelector(".add-option").addEventListener("click", function (e) {
  const text = document.querySelector("#optionText").value;
  const option = document.createElement("option");
  option.value = text.toLowerCase();
  option.textContent = text;

  document.querySelector("#subject").appendChild(option);
  document.querySelector("#optionText").value = "";
  e.preventDefault();
});

// d ) Remove all the options of a select box and then add one option and select it
document
  .querySelector(".removeAll-addOne-and-selectIt")
  .addEventListener("click", function (e) {
    // removing All
    const options = document.querySelectorAll("#subject option");
    options.forEach((option) => {
      option.remove();
    });
    // adding one
    const option = document.createElement("option");
    option.value = "nodejs";
    option.textContent = "Node Js";
    document.querySelector("#subject").appendChild(option);

    // Select It
    option.setAttribute("selected", "selected");
    document.querySelector(".selected-subject").textContent = option.value;
  });

// e ) Dynamically set first options selected using Jquery
$("#subject option:first").attr("selected", "selected");
$(".selected-subject").text($("#subject option:first").val());
